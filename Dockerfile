FROM alpine:3.16.2 as base

FROM base as build
RUN apk add --no-cache go git
RUN git clone https://github.com/sigstore/cosign
WORKDIR cosign
RUN go install ./cmd/cosign

FROM base as cosign
COPY --from=build /root/go/bin/cosign /usr/local/bin/cosign
ENTRYPOINT [ "/usr/local/bin/cosign" ]
